var mapEkb,
    mapEkbIndex,
    geoMap;


function initMap() {
  
  // EKB MAP
  if($('#map-ekb').length) {
    mapEkb = new google.maps.Map(document.getElementById('map-ekb'), {
      center: {lat: 56.835014, lng: 60.610286},
      zoom: 16,
      disableDefaultUI: true,
      styles: [
          {
              "featureType": "administrative.country",
              "elementType": "geometry",
              "stylers": [
                  {
                      "visibility": "simplified"
                  },
                  {
                      "hue": "#ff0000"
                  }
              ]
          }
      ]
    });
  }
  
  // EKB MAP in index
  if($('#map-ekb-index').length) {
    mapEkbIndex = new google.maps.Map(document.getElementById('map-ekb-index'), {
      center: {lat: 56.836916, lng: 60.619706},
      zoom: 16,
      disableDefaultUI: true,
      styles: [
          {
              "featureType": "administrative.country",
              "elementType": "geometry",
              "stylers": [
                  {
                      "visibility": "simplified"
                  },
                  {
                      "hue": "#ff0000"
                  }
              ]
          }
      ]
    });
  }
  
  // GEOGRAPHY MAP
  if($('#geo-map').length) {
    geoMap = new google.maps.Map(document.getElementById('geo-map'), {
      center: {lat: 54.079231, lng: 50.146768},
      zoom: 4,
      disableDefaultUI: true,
      styles: [
                {
                    "featureType": "administrative",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": "-100"
                        }
                    ]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 65
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": "50"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": "-100"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "30"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "40"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "hue": "#ffff00"
                        },
                        {
                            "lightness": -25
                        },
                        {
                            "saturation": -97
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "lightness": -25
                        },
                        {
                            "saturation": -100
                        }
                    ]
                }
            ]
    });
  }
  
  // MARKERS
  // for geo map
  var bigMarker = new google.maps.Marker({
      position: {lat: 50.597186, lng: 71.632233},
      map: geoMap,
      clickable: false,
      icon: {
        url: 'static/i/map-marker.png',
        scaledSize: new google.maps.Size(43, 53)
      }
  });
  var ekbMarker = new google.maps.Marker({
      position: {lat: 56.920997, lng: 74.005279},
      map: geoMap,
      clickable: false,
      icon: {
        url: 'static/i/map-marker_ekb.png'
      }
  });
  var smallMarker = new google.maps.Marker({
      position: {lat: 55.9492, lng: 37.618561},
      map: geoMap,
      clickable: false,
      icon: {
        url: 'static/i/map-marker.png',
        scaledSize: new google.maps.Size(30, 37)
      }
  });
  // in contacts
  var marker = new google.maps.Marker({
      position: {lat: 56.836039, lng: 60.614448},
      map: mapEkb,
      icon: 'static/i/address-marker.png'
  });
  // in index
  var marker = new google.maps.Marker({
      position: {lat: 56.836039, lng: 60.614448},
      map: mapEkbIndex,
      icon: 'static/i/address-marker.png'
  });
  
  
}