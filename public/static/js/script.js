$(document).ready(function(){
  console.log('Start rock!');
  // ANIMATION in index for advantages
  if($('.advantage').length) {
    $(document).animateScroll({
      duration:'1.5',"y": "0", "alpha": "0"
    });
    // One time play animation
    $('.animateScroll').each(function(){
      $(this).unbind('play')
    });
  }
  // INIT CAROUSELS
  $(".crsl-bar").owlCarousel({
    loop: true,
    margin: 90,
    autoWidth:true,
    nav: true,
    center: true,
    autoHeight: true
  });
  $(".main-crsl").owlCarousel({
    loop: true,
    items: 1,
    nav: true,
    autoplay: true,
    autoplayTimeout: 10000
  });
  $(".reviews-crsl").owlCarousel({
    loop: true,
    nav: true,
    autoWidth:true,
    center: true,
    margin: 60
  });
  
  // INDEX TOP CAROUSEL
  if($('.slogan').length && $('.main-crsl')) {
    // to slide
    $('.slogan__item').click(function() {
      $('.main-crsl').trigger('to.owl.carousel', $(this).index() + 1)
    });
    
    // evenys when carousel changed
    $('.main-crsl').on('changed.owl.carousel', function() {
      var activeSlide = $(this).find('.owl-dot.active').index();
      var btn = $('.slogan__item');
      
      btn.each(function() {$(this).removeClass('slogan__item_active');});
      btn.eq(activeSlide - 1).addClass('slogan__item_active');
      if(activeSlide === 0) {
        btn.each(function() {$(this).removeClass('slogan__item_active');});
      }
    });
  }
  
  // FANCY MODALS
  // for reviews crls
  if($('.reviews-crsl').length) {
    $(".reviews-crsl__item").on('click', function() {
      var reviewIndex = $(this).data('review');
      $.fancybox.open({
        src: '#review-' + reviewIndex
      });
    });
  }
  // for consulting modal
  $('.js-consult').on('click', function() {
    $.fancybox.open({src: '#consulting'});
  });
  
  // ACCORDION
  if($('.accordion__toggle').length) {
    $('.accordion__toggle').click( function() {
      if ($(this).hasClass('accordion__toggle_active')) {
        $(this).parent().siblings().fadeOut();
      } else {
        $(this).parent().siblings().fadeIn();
      }
      $(this).toggleClass('accordion__toggle_active');
    });
  }
  // SIDEBAR FUNCTIONS
  if($('.page__sidebar').length) {
    function windowSize(){
      if ($(window).width() <= '768'){
        $('.accordion__toggle').removeClass('accordion__toggle_active');
        $('.accordion__toggle').parent().siblings().fadeOut();
      } else {
        $('.accordion__toggle').addClass('accordion__toggle_active');
        $('.accordion__toggle').parent().siblings().fadeIn();
      }
    }

    $(window).on('load resize', windowSize);
  }
  
  // MOUSE SCROLL
  if($('.js-anchor').length) {
    $('.js-anchor').click(function(e) {
      e.preventDefault();
      var id  = $(this).attr('href'),
      top = $(id).offset().top - 100;
      $('body,html').animate({scrollTop: top}, 1000);
    });
  }
  
  // TOGGLE MENU
  $('.menu-toggle').click(function() {
    $(this).toggleClass('menu-toggle_active')
    $('.main-nav').toggleClass('main-nav_active')
  });
  if($('.header-big__btn-start').length) {
    $('.header-big__btn-start').click(function() {
      $('.menu-toggle').toggleClass('menu-toggle_active')
      $('.main-nav').toggleClass('main-nav_active')
    });
  }
  // FAQ TOGGLE
  if($('.page-faq__item-toggle').length) {
    $('.page-faq__item-toggle').click( function() {
      
      $(this).toggleClass('page-faq__item-toggle_active');
      $(this).parents('.page-faq__item').toggleClass('page-faq__item_active');
    });
  }
  
  // INDEX HEADER
  if($('.header-big').length) {
    $('.header-big__btn-start').click(function() {
      return false
    });
    // changing header
    $(window).scroll(function(){
      if ($(window).scrollTop() > 400) {
          $('.header').removeClass('header-big');
      }
      else {
          $('.header').addClass('header-big');
      }
    });
  }
  // WORK STEPS in index
  if($('.work-step').length) {
    $('.work-step').click(function() {
      $('.work-step').not(this).removeClass('work-step_active');
      $(this).toggleClass('work-step_active');
      if($(this).hasClass('work-step_active')) {
        // z-index
        $('.work-step').each(function() {
          $(this).css({'z-index': ''});
          $(this).children('.work-step__title').css({'color':''})
        });
        $(this).css({'z-index': 1000});
        $(this).children('.work-step__title').css({'color':'transparent'})
        // close all elements
        $('.work-step__content').each(function(){ $(this).fadeOut(); });
        // open this element
        $(this).siblings('.work-step__content').fadeIn();
      } else {
        $(this).children('.work-step__title').css({'color':''});
        $(this).siblings('.work-step__content').fadeOut();
      }
      
    });
  }
  
  // TABS
  $('.tabs__item').on('click', function() {
    $(this).addClass('tabs__item_active').siblings().removeClass('tabs__item_active');
    $(this).closest('.tabs').find('.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
  });
  
  // ADD FILES INTO FORM
  if($('.js-add-file').length) {
    $('.js-add-file').click( function() {
      $(this).parent().append('<div class="link-attachment"><div class="link-attachment__remove" onclick="$(this).parent().remove();"></div>Название файла</div>')
    });
  }
  
  /*---------------
      #Masks
  ---------------*/
  window.sitename = {};
  
  window.sitename.form = ({
    
    init: function(){
      
      var _th = this;

      
      $('form').submit(function(){
                if (!_th.checkForm($(this))) {
                  $('.page-form__error-text').addClass('page-form__error-text_active');
                  return false
                }
            });
    },
    
    checkForm: function(form){
      var checkResult = true;
      form.find('.warning').removeClass('warning');
      form.find('input, textarea, select').each(function(){
        if ($(this).data('req')){
          switch($(this).data('type')){
            case 'tel':
              var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
              if (!re.test($(this).val())){
                  $(this).addClass('warning');
                  checkResult = false;
              }
              break;
          case 'email':
              var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
              if (!re.test($(this).val())){
                  $(this).addClass('warning');
                  checkResult = false;
              }
              break;
          default:
          if ($.trim($(this).val()) === ''){
            $(this).addClass('warning');
                            checkResult = false;
                        }
                        break;
                }
             }
      });
      if ($('.checkbox_front').length) {
        if (!$('.checkbox_front').is(':checked')) {
          $('.checkbox_front').addClass('warning');
          checkResult = false;
        }
      }
      return checkResult;
    }
    
  }).init();
  
  // NUMBER INCREASING EFFECT
  if($('.js-num').length) {
    var number = document.querySelector('.js-num'),
      numberTop = number.getBoundingClientRect().top,
      start = +number.innerHTML, end = +number.dataset.max;

    window.addEventListener('scroll', function onScroll() {
      if(window.pageYOffset > numberTop - window.innerHeight * 0.75 ) {
        this.removeEventListener('scroll', onScroll);
        var sep = $.animateNumber.numberStepFactories.separator(' ')
        $('.js-num').each(function (i) {
          var th = $(this);
          var numb = $(this).data('digits');

          th.animateNumber({
            number: numb,
            numberStep: sep
          }, 5000, function () {
            th.addClass('complete');
          });
        });
      }
    });
  }

});

