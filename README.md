## Посмотреть верстку по ссылке

* [Главная](https://zemil.bitbucket.io/public/index-page.html)
* [404](https://zemil.bitbucket.io/public/404.html)
* [Статья](https://zemil.bitbucket.io/public/article.html)
* [Статьи](https://zemil.bitbucket.io/public/articles.html)
* [Контакты](https://zemil.bitbucket.io/public/contacts.html)
* [Default](https://zemil.bitbucket.io/public/default.html)
* [Faq](https://zemil.bitbucket.io/public/faq.html)
* [Услуга](https://zemil.bitbucket.io/public/usluga.html)
* [Услуги](https://zemil.bitbucket.io/public/uslugi.html)
* [Вакансия](https://zemil.bitbucket.io/public/vacancy.html)

### To run

- Execute `npm install` from this directory to install dev dependencies.
- Execute `gulp` to run all tasks, launch the browser sync local server and watch for changes.